#!/bin/bash

# Note: this is a sample project, please replace the below with the location of your Coherence JAR and littlegrid JAR

# Note: this can be evolved to be more sophisticated to pick up the below from a properties file or environment variables


COHERENCE_JAR=/home/jhall/maven/repository/com/oracle/coherence/coherence/3.7.1.8/coherence-3.7.1.8.jar
LITTLEGRID_JAR=/home/jhall/maven/repository/org/littlegrid/littlegrid/2.15.2/littlegrid-2.15.2.jar
APPLICATION_JARS=./small-sample-datagrid-cluster/target/small-sample-datagrid-cluster-1.0-SNAPSHOT.jar

LITTLEGRID_MAIN_CLASS_NAME=org.littlegrid.app.StorageDisabledClientReplApp

JVM_SERVER_FLAG=-server
JVM_MAXIMUM_MEMORY=512m
JVM_INITIAL_MEMORY=512m
JVM_PERM_SPACE_MEMORY=128m

JVM_ARGS="$JVM_SERVER_FLAG -Xms$JVM_INITIAL_MEMORY -Xmx$JVM_MAXIMUM_MEMORY -XX:MaxPermSize=$JVM_PERM_SPACE_MEMORY"


CLASS_PATH="-cp $COHERENCE_JAR:$LITTLEGRID_JAR:$APPLICATION_JARS"

JAVA_COMMAND_LINE="$JVM_ARGS $CLASS_PATH $LITTLEGRID_MAIN_CLASS_NAME"

echo "Starting littlegrid using: $JAVA_COMMAND_LINE"

java $JAVA_COMMAND_LINE
