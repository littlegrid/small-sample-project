#!/bin/bash

mvn clean install

STATUS=$?

if [ $STATUS -eq 0 ]; then
    echo "Successful"

    ./launch-mini-cluster.sh
else
    echo
    echo "--> Maven step failed, launching of cluster aborted because of a compilation problem or test(s) failed <--"
    echo
fi
